/**
 * The number of additional lines to include before and after the highlighted code block.
 * This padding provides context around the specific lines of interest.
 * @constant {number}
 * @default 3
 */
export const linesPadding = 3;
